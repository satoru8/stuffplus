COLOR_MAGENTA='\033[0;35m'
COLOR_CYAN='\033[0;36m'
COLOR_BLUE='\033[0;34m'
COLOR_YELLOW='\033[0;33m'
COLOR_RED='\033[0;31m'
COLOR_GREEN='\033[0;32m'
COLOR_RESET='\033[0m'

# aliases
alias i="sudo dnf install" 2>/dev/null
alias u="sudo dnf update" 2>/dev/null
alias r="sudo dnf remove" 2>/dev/null
alias s="sudo dnf search" 2>/dev/null
alias clr="clear" 2>/dev/null
alias user="sudo chown sato" 2>/dev/null
alias chgrub="sudo grub2-mkconfig -o /boot/grub2/grub.cfg" 2>/dev/null
alias updateterm="sudo cp /mnt/TForce2TB/Repo/stuffplus/bashProfile/spicyprofile.sh /etc/profile.d/" 2>/dev/null

# functions
function i() {
    sudo dnf install $@
}

function u() {
    sudo dnf update $@
}

function r() {
    sudo dnf remove $@
}

function s() {
    sudo dnf search $@
}

# Welcome message
echo -e "${COLOR_BLUE}===============================================================================${COLOR_RESET}"
echo -e "${COLOR_BLUE}==${COLOR_RESET}                           ${COLOR_CYAN}Welcome to SpicyKatsu!${COLOR_RESET}                          ${COLOR_BLUE}==${COLOR_RESET}"
echo -e "${COLOR_BLUE}===============================================================================${COLOR_RESET}"
echo -e "Type '${COLOR_YELLOW}i${COLOR_RESET}' to install packages, '${COLOR_YELLOW}u${COLOR_RESET}' to update, '${COLOR_YELLOW}r${COLOR_RESET}' to remove, or '${COLOR_YELLOW}s${COLOR_RESET}' to search."
echo

# neofetch
neofetch --ascii_distro Fedora_small --ascii_colors 2 9


# Theme Stuff
GRUB_THEME_PATH=$(grep -oP '(?<=^GRUB_THEME=").*(?=")' /etc/default/grub)
GRUB_THEME_DIR=$(dirname "$GRUB_THEME_PATH")
GRUB_THEME_NAME=$(basename "$GRUB_THEME_DIR")
SHELL_THEME=$(dconf read /org/gnome/shell/extensions/user-theme/name | tr -d "'")
LEGACY_SHELL_THEME=$(gsettings get org.gnome.desktop.interface gtk-theme | tr -d "'")

echo -e "GRUB theme: ${COLOR_MAGENTA}$GRUB_THEME_NAME${COLOR_RESET}"
echo -e "Shell Theme: ${COLOR_MAGENTA}$SHELL_THEME${COLOR_RESET}"
echo -e "Legacy Shell theme: ${COLOR_MAGENTA}$LEGACY_SHELL_THEME${COLOR_RESET}" 

echo