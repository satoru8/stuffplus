#!/bin/bash

# Set display and Xauthority variables
export DISPLAY=:0
export XAUTHORITY=/run/user/1000/gdm/Xauthority
echo "Display and Xauthority variables set."

# Paths
NV_SETTINGS="/usr/bin/nvidia-settings"
GRUB_CONFIG="/etc/default/grub"

# Check if Coolbits 4 is enabled and enable it if necessary
check_coolbits_enabled() {
    $NV_SETTINGS -c :0 -q '[gpu:0]/GPUGraphicsClockOffset' 2>/dev/null
}

echo "Checking Coolbits state..."
if ! check_coolbits_enabled; then
    echo "Coolbits 4 is not enabled. Enabling Coolbits 4..."
    sudo sed -i 's/^GRUB_CMDLINE_LINUX="\(.*\)"$/GRUB_CMDLINE_LINUX="\1 nvidia-drm.modeset=1 coolbits=4"/' "$GRUB_CONFIG" && \
    sudo grub2-mkconfig -o /boot/grub2/grub.cfg && \
    echo "Coolbits 4 enabled successfully."
else
    echo "Coolbits 4 is already enabled."
fi

# Set GPU fan control state
echo "Checking GPU fan control state..."
current_state=$($NV_SETTINGS -c :0 -q '[gpu:0]/GPUFanControlState' | grep 'Attribute' | awk '{print $NF}')
echo "Current GPU fan control state: $current_state"
if [[ "$current_state" == "1." ]]; then
    echo "GPU fan control state is already set to '1'."
elif [[ "$current_state" == "0." ]]; then
    echo "Current GPU fan control state is '0', setting it to '1'..."
    if $NV_SETTINGS -c :0 -a '[gpu:0]/GPUFanControlState=1'; then
        echo "GPU fan control state set to '1' successfully."
    else
        echo "Failed to set GPU fan control state."
        # exit 1
    fi
else
    echo "Unknown GPU fan control state. Exiting."
    # exit 1
fi

# Change GRUB theme
echo "Changing GRUB theme..."
CURRENT_THEME=$(grep '^GRUB_THEME=' "$GRUB_CONFIG" | cut -d'"' -f2)

# Load themes list
THEMES_LIST="/usr/share/grub/themes/themes.txt"
if [ ! -f "$THEMES_LIST" ]; then
    echo "Themes list not found. Exiting."
    exit 1
fi

# Check current theme
readarray -t ALL_THEMES < "$THEMES_LIST"
if [ -z "$CURRENT_THEME" ]; then
    echo "No current GRUB theme found."
else
    THEMES=("${ALL_THEMES[@]/$CURRENT_THEME}")
fi

# Set random theme
if [ ${#THEMES[@]} -gt 0 ]; then
    RANDOM_THEME=${THEMES[$((RANDOM % ${#THEMES[@]}))]}
    echo "Current GRUB theme: $CURRENT_THEME"
    cp "$GRUB_CONFIG" "$GRUB_CONFIG.bak" && \
    echo "Backup of GRUB configuration created: $GRUB_CONFIG.bak" && \
    sed -i "s|^GRUB_THEME=.*|GRUB_THEME=\"$RANDOM_THEME\"|" "$GRUB_CONFIG" && \
    echo "GRUB theme updated to: $RANDOM_THEME" && \
    grub2-mkconfig -o /boot/grub2/grub.cfg && \
    echo "GRUB configuration updated successfully." || \
    echo "Failed to set GRUB theme."
else
    echo "No alternative themes available."
    exit 1
fi
